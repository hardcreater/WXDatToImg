﻿namespace WX_DatToImg
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.filePathText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFolderBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.outPathText = new System.Windows.Forms.TextBox();
            this.convertBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.convertNum = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // filePathText
            // 
            this.filePathText.Location = new System.Drawing.Point(86, 29);
            this.filePathText.Name = "filePathText";
            this.filePathText.Size = new System.Drawing.Size(409, 25);
            this.filePathText.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "文件路径";
            // 
            // openFolderBtn
            // 
            this.openFolderBtn.Location = new System.Drawing.Point(511, 27);
            this.openFolderBtn.Name = "openFolderBtn";
            this.openFolderBtn.Size = new System.Drawing.Size(121, 29);
            this.openFolderBtn.TabIndex = 2;
            this.openFolderBtn.Text = "选择目录";
            this.openFolderBtn.UseVisualStyleBackColor = true;
            this.openFolderBtn.Click += new System.EventHandler(this.openFolder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "输出路径";
            // 
            // outPathText
            // 
            this.outPathText.Location = new System.Drawing.Point(86, 72);
            this.outPathText.Name = "outPathText";
            this.outPathText.Size = new System.Drawing.Size(409, 25);
            this.outPathText.TabIndex = 4;
            // 
            // convertBtn
            // 
            this.convertBtn.Location = new System.Drawing.Point(511, 69);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(121, 27);
            this.convertBtn.TabIndex = 6;
            this.convertBtn.Text = "开始转换";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.convertBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "已转换：";
            // 
            // convertNum
            // 
            this.convertNum.AutoSize = true;
            this.convertNum.Location = new System.Drawing.Point(83, 117);
            this.convertNum.Name = "convertNum";
            this.convertNum.Size = new System.Drawing.Size(15, 15);
            this.convertNum.TabIndex = 9;
            this.convertNum.Text = "0";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "所有文件|*.*|微信图片|*.dat";
            // 
            // openFolderDialog
            // 
            this.openFolderDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel1.LinkColor = System.Drawing.Color.Black;
            this.linkLabel1.Location = new System.Drawing.Point(543, 113);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(89, 23);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "gitee.com";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(645, 144);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.convertNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.convertBtn);
            this.Controls.Add(this.outPathText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.openFolderBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filePathText);
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "微信图片转换";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.LinkLabel linkLabel1;

        #endregion

        private System.Windows.Forms.TextBox filePathText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openFolderBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox outPathText;
        private System.Windows.Forms.Button convertBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label convertNum;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.FolderBrowserDialog openFolderDialog;
    }
}

